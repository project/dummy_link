<?php

/**
 * @file
 * This is an example file for how you could override theme_menu_link().
 *
 * NOTICE! THIS FILE IS NOT BEING INCLUDED AND IS WITHOUT FUNCTIONALITY.
 *
 * @see theme_menu_link()
 */

/**
 * Implements theme_menu_link().
 *
 * Replace the <a> tag with a <span> around <nolink>.
 *
 * This hook is never being called, but feel free to copy it to your own
 * template.php file and modify it if needed.
 */
function dummy_link_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);

  // If the link is a nolink one (see hook_menu() in reload_f5.module), then
  // do some extra stuff and overwrite $output.
  if (preg_match('{<nolink>}', $element['#href'])) {
    $output = '<span>' . strip_tags($output) . '</span>';
    $element['#attributes']['class'][] = 'nolink';
  }

  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}
