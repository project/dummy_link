README.txt
==========

INTRODUCTION
------------

This is a very simple module. It provides a link path (<nolink>) and a file
with an example of how you can override theme_menu_link() to make the <nolink>
into a <span> element instead of an <a> element.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will provide <nolink> path that can be used for menu items.
